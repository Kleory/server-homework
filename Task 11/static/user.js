async function handlerUser() {
  let adr = location.pathname.split("/");

  let res = await fetch(`http://localhost:3000/users/data/${adr[2]}`);
  let result = await res.json();

  let h1 = document.createElement("h1");
  h1.innerHTML =
    "Hello " +
    result.userData[0].name +
    " " +
    (result.userData[0].lastname || " ");
  document.body.prepend(h1);
}

document.addEventListener("DOMContentLoaded", handlerUser);

deleteUser.onsubmit = async (e) => {
  e.preventDefault();

  let adr = location.pathname.split("/");

  await fetch(`http://localhost:3000/users/data/${adr[2]}`, {
    method: "DELETE",
  });

  location.assign("/users");
  alert("Запись удалена");
};

edit.onsubmit = async (e) => {
  e.preventDefault();

  const value1 = edit.userName.value;
  const value2 = edit.userSurname.value;
  const d = { firstName: value1, lastName: value2 };

  let adr = location.pathname.split("/");

  let res = await fetch(`http://localhost:3000/users/data/${adr[2]}`, {
    method: "PUT",
    body: JSON.stringify(d),
    headers: {
      "Content-Type": "application/json; charset = UTF-8 ",
    },
  });
  let result = await res.json();

  alert("Запись изменена", result);
};
