formElem.onsubmit = async (e) => {
  e.preventDefault();
  const value1 = formElem.userName.value;
  const value2 = formElem.userSurname.value;
  const d = { firstName: value1, lastName: value2 };

  let response = await fetch("http://localhost:3000/users", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(d),
  });

  let result = await response.json();

  alert("Запись добавлена", result);
};

async function handlerVisible() {
  let res = await fetch("http://localhost:3000/users/data");
  let result = await res.json();

  let ul = document.createElement("ul");

  result.d.map((item, key) => {
    let li = document.createElement("li");
    let a = document.createElement("a");
    a.innerHTML = item.name + " " + (item.lastname || " ");
    a.href = `users/${item.id}`;
    li.append(a);
    ul.append(li);
  });
  document.body.append(ul);
}

document.addEventListener("DOMContentLoaded", handlerVisible);
