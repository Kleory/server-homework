const express = require("express");
const path = require("path");
var router = express.Router();

const PORT = process.env.PORT || 3000;
const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(express.json());
app.use(express.static(path.resolve(__dirname, "static")));

app.use("/", require("./routes/index"));
app.use("/users", require("./routes/users"));

app.listen(PORT, () => {
  console.log(`Server has been started on port ${PORT}...`);
});
