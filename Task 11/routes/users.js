const { Router } = require("express");
const path = require("path");
const router = Router();

const { Pool } = require("pg");
const pool = new Pool({
  database: "postgres",
  user: "postgres",
  password: "123",
  host: "localhost",
  port: 5432,
});

router.get("/", (req, res, next) => {
  res.render("users", { title: "users" });
});

router.post("/", async (req, res) => {
  try {
    const data = req.body;
    console.log("data", req.body);
    const client = await pool.connect();
    await client.query('INSERT INTO "users" (name, lastName) VALUES ($1, $2)', [
      data.firstName,
      data.lastName,
    ]);
    res.status(201).json({ data });
  } catch (error) {
    res.status(500).json({ message: "Что-то не так с post запросом" });
  }
});

router.get("/data", async (req, res, next) => {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT * FROM "users"', []);
    d = result.rows;
    res.status(201).json({ d });
  } catch (error) {
    res.status(500).json({ message: "Что-то не так с get запросом" });
  }
});

router.get("/data/:id", async (req, res, next) => {
  try {
    const client = await pool.connect();
    const result = await client.query('SELECT * FROM "users" WHERE id = $1', [
      req.params.id,
    ]);
    userData = result.rows;
    res.status(201).json({ userData });
  } catch (error) {
    res.status(500).json({ message: "Что-то не так с get запросом" });
  }
});

router.delete("/data/:id", async (req, res, next) => {
  try {
    const client = await pool.connect();
    await client.query('DELETE FROM "users" WHERE id = $1', [req.params.id]);
    res.status(201).end();
  } catch (error) {
    res.status(500).json({ message: "Что-то не так с delete запросом" });
  }
});

router.put("/data/:id", async (req, res, next) => {
  try {
    const data = req.body;
    const client = await pool.connect();
    const result = await client.query(
      'UPDATE "users" SET name=$1, lastName=$2 WHERE id = $3',
      [data.firstName, data.lastName, req.params.id]
    );
    res.status(201).json({ data });
  } catch (error) {
    res.status(500).json({ message: "Что-то не так с put запросом" });
  }
});

router.get("/:id", async (req, res, next) => {
  res.render("userId", { title: "user" });
});

module.exports = router;
